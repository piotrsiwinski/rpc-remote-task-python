#!/usr/bin/env python3
import asyncio
from asyncio.subprocess import PIPE
from functools import partial

from rpcudp.protocol import RPCProtocol

# Any methods starting with "rpc_" are available to clients.
from util import get_server_program_args


class RPCServer(RPCProtocol):
    senders_processes = {}

    async def submit_process(self, sender, command):
        process = await asyncio.create_subprocess_shell(command, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        self.senders_processes[sender] = process
        return process

    # Metoda wywołana przez klienta
    async def rpc_exec_command(self, sender, command):
        print("Received command to exec:\t`%s`\tfrom: %s:%s" % (command, sender[0], sender[1]))

        submitted_process = await self.submit_process(sender, command)
        # Tworzymy 3 nowe zadania; 2 do obserwowania std i stderr, oraz 1 po wykonaniu procesu
        asyncio.ensure_future(self.observe_stdout(sender, submitted_process))
        asyncio.ensure_future(self.observe_stderr(sender, submitted_process))
        asyncio.ensure_future(self.on_process_end(sender, submitted_process))

    async def observe_stdout(self, sender, process):
        # funkcja monitorująca std i przekazująca częściową funkcję, który wywoła rpc_consume_stdout u klienta
        await self.observe_stream(process.stdout, partial(protocol.consume_stdout, sender))


    async def observe_stderr(self, sender, process):
        # funkcja monitorująca stderr i przekazująca częściową funkcję, który wywoła rpc_consume_stderr u klienta
        await self.observe_stream(process.stderr, partial(protocol.consume_stderr, sender))

    async def observe_stream(self, stream, callback):
        while True:
            line = await stream.readline()
            if line:
                callback(line)
            else:
                break

    async def on_process_end(self, sender, process):
        result = await process.wait()
        print("Sending response to:\t%s:%s" % (sender[0], sender[1]))
        del self.senders_processes[sender]
        # wywołuje metodę u klienta z statusem zadania
        protocol.consume_return_code(sender, result)


event_loop = asyncio.get_event_loop()
args = get_server_program_args()
listen = event_loop.create_datagram_endpoint(RPCServer, local_addr=(args.server_address, args.server_port))
transport, protocol = event_loop.run_until_complete(listen)
event_loop.run_forever()
