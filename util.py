import argparse
import os


def get_client_program_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('command', help='shell command to run on the remote server')
    parser.add_argument('--host-address',
                        help='Address of the RPC server. Default address is 127.0.0.1 or HOST_ADDRESS environment variable',
                        default=os.environ.get('HOST_ADDRESS', '127.0.0.1'))
    parser.add_argument('--host-port', type=int,
                        help='Port of the server. Default is 1234 or HOST_PORT environment variable',
                        default=os.environ.get('HOST_PORT', 1234))
    parser.add_argument('--client-address',
                        help='Address of the RPC client. Default is 127.0.0.1 or CLIENT_ADDRESS environment variable',
                        default=os.environ.get('CLIENT_ADDRESS', '127.0.0.1'))
    parser.add_argument('--client-port', type=int,
                        help='Port of the client. Default port number will be created by OS CLIENT_PORT environment variable',
                        default=os.environ.get('CLIENT_PORT', None))
    return parser.parse_args()


def get_server_program_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--server-address',
                        help='Server address. Default is to 127.0.0.1 or SERVER_ADDRESS environment variable',
                        default=os.environ.get('SERVER_ADDRESS', '127.0.0.1'))
    parser.add_argument('--server-port', type=int,
                        help='Port of the server. Defaults to 1234 or SERVER_PORT environment variable',
                        default=os.environ.get('SERVER_PORT', 1234))
    return parser.parse_args()
