#!/usr/bin/env python3

import asyncio
import sys

from rpcudp.protocol import RPCProtocol
from util import get_client_program_args


# Any methods starting with "rpc_" are available to clients.
class RpcListener(RPCProtocol):

    def rpc_consume_stdout(self, sender, output):
        print("Received response in std from %s:%s\n" % (sender[0], sender[1]))
        print(output.decode("utf-8").rstrip('\n'), flush=True)

    def rpc_consume_stderr(self, sender, output):
        print("Received response in stderr from %s:%s\n" % (sender[0], sender[1]))
        print(output.decode("utf-8").rstrip('\n'), file=sys.stderr, flush=True)

    def rpc_consume_return_code(self, sender, retval):
        print("Received response from server: Return val is %i" % retval)
        global return_code
        return_code = retval
        global event_loop
        event_loop.stop()


async def exec_command_async(protocol, address, command):
    await protocol.exec_command(address, command)


return_code = 1
event_loop = asyncio.get_event_loop()


# Start local UDP server to be able to handle responses
def main():
    args = get_client_program_args()

    listen = event_loop.create_datagram_endpoint(RpcListener, local_addr=(args.client_address, args.client_port))
    transport, protocol = event_loop.run_until_complete(listen)

    # Call remote UDP server and pass command
    # wywołanie metody z servera rpc_exec_shell
    coroutine = exec_command_async(protocol, (args.host_address, args.host_port), args.command)
    event_loop.run_until_complete(coroutine)

    event_loop.run_forever()
    sys.exit(return_code)


if __name__ == "__main__":
    main()
