# RPC RemoteTask
An implementation of remote shell command execution using RPCUDP Python library.
Server sends in response submitted task return code.

## Build prerequisites
To run this you will need ```Python 3.5 or later```
 
#### Download requirements
```
pip install requirements.txt
```

## Usage
#### Running server
```consoleLine
python3 server.py
```
#### Running client 
``python3 client.py "REMOTE_COMMAND args"``